const express =  require('express');
const controller = require('../controller/controller')

const router = express.Router();
router.get('/api/sub/:num1/:num2' , controller.getSubtraction);
router.get('/api/add/:num1/:num2' , controller.getAddition);
router.get('/api/multiply/:num1/:num2' , controller.getMultiply);
router.get('/api/divide/:num1/:num2' , controller.getDivision);


module.exports = router;
