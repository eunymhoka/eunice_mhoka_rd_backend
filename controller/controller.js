const express = require("express");
const bcrypt = require("bcryptjs");

const { sql, poolPromise } = require('../database/db')
const fs = require('fs');
var rawdata = fs.readFileSync('./query/queries.json');
var queries = JSON.parse(rawdata);

var session = require('express-session');
const { max } = require("moment");

class MainController {

  async getAddition(req, res) {
    try {
      if (req.body.num1 != null && req.body.num2 != null && req.body.result != null) {
        var addition = num1 + num2;
        const pool = await poolPromise
        const result = await pool.request()

          .input('num1', sql.Int, req.body.num2)
          .input('num2', sql.VarChar, req.body.num2)
          .input('result', sql.VarChar, req.body.addition)
          .query(queries.getAddition)
        res.json(result)
      }
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }
  async getMultiply(req, res) {
    try {
      if (req.body.num1 != null && req.body.num2 != null && req.body.result != null) {
        var multiply = num1 * num2;
        const pool = await poolPromise
        const result = await pool.request()

          .input('num1', sql.Int, req.body.num2)
          .input('num2', sql.VarChar, req.body.num2)
          .input('result', sql.VarChar, req.body.multiply)
          .query(queries.getMultiply)
        res.json(result)
      }
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }
  async getSubtraction(req, res) {
    try {
      if (req.body.num1 != null && req.body.num2 != null && req.body.result != null) {
        var subtraction = num1 + num2;
        const pool = await poolPromise
        const result = await pool.request()

          .input('num1', sql.Int, req.body.num2)
          .input('num2', sql.VarChar, req.body.num2)
          .input('result', sql.VarChar, req.body.subtraction)
          .query(queries.getSubtraction)
        res.json(result)
      }
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }
  async getDivision(req, res) {
    try {
      if (req.body.num1 != null && req.body.num2 != null && req.body.result != null) {
        var division = num1 + num2;
        const pool = await poolPromise
        const result = await pool.request()

          .input('num1', sql.Int, req.body.num2)
          .input('num2', sql.VarChar, req.body.num2)
          .input('result', sql.VarChar, req.body.division)
          .query(queries.getDivision)
        res.json(result)
      }
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

 


}

const controller = new MainController()
module.exports = controller;

